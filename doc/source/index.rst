.. nminds-x4-submaker documentation master file, created by
   sphinx-quickstart on Mon Dec 29 11:07:32 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to nminds-x4-submaker's documentation!
===========================================

Contents:

.. toctree::
   :maxdepth: 2

   license.rst
   readme.rst
   modules.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

