Readme
======

Program
-------

- Nazwa: nminds-x4-submaker
- Wersja: 1.0
- Lokalizacja kodu: src/
	
Autorzy
-------

Michał Denkiewicz (michal.denkiewicz@gmail.com)

Licencja
--------
    
(c) 2014 Instytut Psychologii Polskiej Akademii Nauk.
Wszelkie prawa zastrzeżone.

Pełny tekst licencji znajduje się w pliku LICENSE.polish

Opis
----

Program nminds-x4-submaker jest narzędziem pomocniczym do programu 'nminds-x4', używanego do przeprowadzania eksperymentów psychologicznych dotyczących podejmowania decyzji w grupach czteroosobowych (więcej informacji znajduje się w dokumentacji programu 'nminds-x4'). 

Program przetwarza dane z plików wynikowych utworzonych w czasie eksperymentu i tworzy napisy oraz metadane dotyczące rozdziałów dla nagrań wideo z sesji eksperymentalnych. Napisy i rozdziały ułatwiają orientację w przebiegu badania osobom oglądającym materiał wideo.

Wymagania
---------

* Python 2.7.6 (32bit)

Użytkowanie
----------------
::

    submaker.py [-h] katalog_danych plik_czasow katalog_wyjsciowy [katalog_metadanych]

    argumenty pozycyjne:
      katalog_danych      Katalog z danymi eksperymentalnymi.
      plik_czasow         Plik .csv z przesunięciami czasowymi nagrań względem zegarów programu kontrolującego eksperyment.
      katalog_wyjsciowy   Katalog, do ktorego zostaną zapisane wyniki działania programu.
      katalog_metadanych  Katalog z metadanymi plików nagrań. Pominięcie spowoduje, że program pominie generowanie metadanych plików wideo.

    argumenty opcjonalne:
      -h, --help          Wyświetla krótką pomoc programu, w języku angielskim.

Przykład użycia znaleźć można w katalogu test/

Dokumentacja
------------
    
Sposób instalacji oraz dokumentacja użytkownika znajudją się w pliku manual/build/instrukcja.pdf (plik ten może zostać wygenerowany poleceniem "make manual").

Dokumentacja programisty (musi być skompilowana narzędziem sphinx):

- doc/build/html/index.html - format html
- doc/build/latex/nminds-x4-submaker-doc.pdf - format pdf

Dokumentacja programisty została wygenerowana przy pomocy programu sphinx - źródła znajdują się w katalogu doc/source/

Testy
-----

Proste środowisko testowe oraz przykładowe dane zawarte są w katalogu "test". Skrypty testowe generują pliki wynikowe do katalogu "out" i następnie porównują je z oczekiwanymi z katalogu "expected", przy użyciu polecenia diff (UNIX) lub fc (Windows).

Testy jednostkowe, korzystające z py.test, zlokalizowane są w katalogu src/tests i możne je uruchomić poleceniem "make test".

Różne
---------------

Katalog misc zawiera:

- Skrypty powłoki Windows używane (wraz z programem ffpeg, https://www.ffmpeg.org/) do konwersji nagrań sesji eksperymentalnych. Przed ich użyciem konieczne jest wprowadzenie do nich ścieżek odpowiednich dla systemu, na którym mają być uruchomione.
- Pliki wynikowe programu nminds-x4-submaker uruchomionego dla nagrań z oyrginalnego eksperymentu.
- Plik times.csv zawierający przesunięcia zagara programu nminds-x4 w stosunku do czasu nagrań (z oryginalnego eksperymentu).