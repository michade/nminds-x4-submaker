# Makefile for nminds-x4-submaker

SPHINXAPIDOC=sphinx-apidoc
INSTALLDIR=./install

.PHONY: help clean-install clean-pyc clean-tests sphinx-apidoc sphinx-build docs manual install-sources install test

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  install    to create a directory ($(INSTALLDIR)) to copy to an installation medium"
	@echo "  docs       to build the programmer's documentation"
	@echo "  manual     to build the user manual (in Polish)"
	@echo "  clean      to remove intermediate files"
	@echo "  test       to run py.test unit tests"

clean-install:
	rm -rf $(INSTALLDIR)

clean-pyc:
	find . -name "*.pyc" -type f -delete

clean-tests:
	rm -f ./test/out/*

clean: clean-install clean-pyc clean-tests
	$(MAKE) -C ./manual clean
	$(MAKE) -C ./doc clean
	@echo
	@echo "Cleaning finished."

sphinx-apidoc:
	$(SPHINXAPIDOC) -o ./doc/source ./src

sphinx-build:
	$(MAKE) -C ./doc html
	@echo
	@echo "Done building html docs."
	$(MAKE) -C ./doc latexpdf
	@echo
	@echo "Done building pdf docs."

docs: sphinx-apidoc sphinx-build
	@echo
	@echo "Documentation build finished."

manual:
	$(MAKE) -C ./manual

install-sources: clean
	[ -d $(INSTALLDIR) ] || mkdir $(INSTALLDIR)
	find . -maxdepth 1 -type f -exec cp {} $(INSTALLDIR) \;
	cp -r src $(INSTALLDIR)
	cp -r test $(INSTALLDIR)
	cp -r doc $(INSTALLDIR)
	cp -r manual $(INSTALLDIR)
	find $(INSTALLDIR) -name ".gitignore" -type f -delete

install: install-sources docs manual
	mkdir -p $(INSTALLDIR)/doc/build
	cp -r doc/build/html $(INSTALLDIR)/doc/build
	mkdir -p $(INSTALLDIR)/doc/build/latex
	cp doc/build/latex/nminds-x4-submaker.pdf $(INSTALLDIR)/doc/build/latex
	mkdir -p $(INSTALLDIR)/manual/build
	cp manual/build/instrukcja.pdf $(INSTALLDIR)/manual/build
	cp -r misc $(INSTALLDIR)
	@echo
	@echo "Installation direcotry created : $(INSTALLDIR)"

test:
	PYTHONPATH=src/ py.test
