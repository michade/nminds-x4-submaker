REM
REM Make a set of sample converted videos - with different sizes, quality etc.
REM

@ECHO off
SETLOCAL ENABLEDELAYEDEXPANSION

SET FFM=D:\ffmpeg\bin\ffmpeg.exe
SET OUTDIR=D:\nminds-x4-2014-nagrania\samples
SET INDIR=D:\nminds-x4-2014-nagrania\vid
SET TGTFILE=sesja015-20140407133824
SET INF=!INDIR!\!TGTFILE!.MTS
SET OUTF=!OUTDIR!\!TGTFILE!

IF NOT EXIST !OUTDIR! (
	ECHO Output direcotry !OUTDIR! doesn not exist, attempting to create...
	MKDIR !OUTDIR!
	ECHO Output direcotry !OUTDIR! created.
)

ECHO Output direcotry: !OUTDIR!

IF EXIST !INDIR!/ (
	ECHO Input directory: !INDIR!
) ELSE (
	ECHO Cannot access input directory !INDIR!, exiting script.
	EXIT /B
)

IF NOT EXIST !INF! (
	ECHO Cannot access input file !INF!, exiting script.
	EXIT /B
)

ECHO Converting !INF!...

SET VOPTS=

%FFM% -i "!INF!" -vcodec libxvid !VOPTS! -s 720x540 -acodec copy "!OUTF!_540_xvid.mp4"
%FFM% -i "!INF!" -vcodec libxvid !VOPTS! -s 720x540 -acodec copy "!OUTF!_540_xvid.avi"
%FFM% -i "!INF!" -vcodec libxvid !VOPTS! -s 720x540 -acodec copy "!OUTF!_540_xvid.mkv"
%FFM% -i "!INF!" -vcodec libx264 !VOPTS! -s 720x540 -acodec copy "!OUTF!_540_x264.mp4"
%FFM% -i "!INF!" -vcodec libx264 !VOPTS! -s 720x540 -acodec copy "!OUTF!_540_x264.avi"
%FFM% -i "!INF!" -vcodec libx264 !VOPTS! -s 720x540 -acodec copy "!OUTF!_540_x264.mkv"

%FFM% -i "!INF!" -vcodec libxvid !VOPTS! -s 960x720 -acodec copy "!OUTF!_720_xvid.mp4"
%FFM% -i "!INF!" -vcodec libxvid !VOPTS! -s 960x720 -acodec copy "!OUTF!_720_xvid.avi"
%FFM% -i "!INF!" -vcodec libxvid !VOPTS! -s 960x720 -acodec copy "!OUTF!_720_xvid.mkv"
%FFM% -i "!INF!" -vcodec libx264 !VOPTS! -s 960x720 -acodec copy "!OUTF!_720_x264.mp4"
%FFM% -i "!INF!" -vcodec libx264 !VOPTS! -s 960x720 -acodec copy "!OUTF!_720_x264.avi"
%FFM% -i "!INF!" -vcodec libx264 !VOPTS! -s 960x720 -acodec copy "!OUTF!_720_x264.mkv"

%FFM% -i "!INF!" -vcodec libxvid !VOPTS! -acodec copy "!OUTF!_1080_xvid.mp4"
%FFM% -i "!INF!" -vcodec libxvid !VOPTS! -acodec copy "!OUTF!_1080_xvid.avi"
%FFM% -i "!INF!" -vcodec libxvid !VOPTS! -acodec copy "!OUTF!_1080_xvid.mkv"
%FFM% -i "!INF!" -vcodec libx264 !VOPTS! -acodec copy "!OUTF!_1080_x264.mp4"
%FFM% -i "!INF!" -vcodec libx264 !VOPTS! -acodec copy "!OUTF!_1080_x264.avi"
%FFM% -i "!INF!" -vcodec libx264 !VOPTS! -acodec copy "!OUTF!_1080_x264.mkv"


ECHO Done converting !INF!.
