REM
REM Convert .MTS files (original session recordings format) to mp4.
REM

@ECHO off
SETLOCAL ENABLEDELAYEDEXPANSION

SET FFM=D:\ffmpeg\bin\ffmpeg.exe
SET INDIR=D:\nminds-x4-2014-nagrania\vid
SET OUTDIR=D:\nminds-x4-2014-nagrania\converted

IF EXIST !INDIR!/ (
	ECHO Input directory: !INDIR!
) ELSE (
	ECHO Cannot access input directory !INDIR!, exiting script.
	EXIT /B
)

IF NOT EXIST !OUTDIR! (
	ECHO Output direcotry !OUTDIR! does not exist, attempting to create...
	MKDIR !OUTDIR!
	ECHO Output direcotry !OUTDIR! created.
)

ECHO Output direcotry: !OUTDIR!

FOR %%f IN (!INDIR!\*.MTS) DO (
	SET INF=%%f
	SET OUTF=!OUTDIR!\%%~nf_1440.mp4
	IF EXIST !OUTF! (
		ECHO Output file !OUTF! already exists, skipping.
	) ELSE (
		ECHO Converting !INF!...
		%FFM% -i "!INF!" -vcodec libx264 -acodec copy "!OUTF!"
		ECHO Done converting !INF!.
	)
)

ECHO All done.