REM 
REM Creates subtitles and new video files with chapter markers.
REM 

@ECHO off
SETLOCAL ENABLEDELAYEDEXPANSION

SET FFM=D:\ffmpeg\bin\ffmpeg.exe
SET PYTHON=C:\Python27\python.exe

REM SET INDIR=D:\nminds-x4-2014-nagrania\test
REM SET OUTDIR=D:\nminds-x4-2014-nagrania\test
SET INDIR=D:\nminds-x4-2014-nagrania\converted
SET OUTDIR=D:\nminds-x4-2014-nagrania\withchapters
SET TIMESF=!INDIR!\times.csv
SET DATADIR=U:\nminds-x4-2014\nminds-x4-data\raw

IF EXIST !INDIR!/ (
	ECHO Input directory: !INDIR!
) ELSE (
	ECHO Cannot access input directory !INDIR!, exiting script.
	EXIT /B
)

IF NOT EXIST !OUTDIR! (
	ECHO Output direcotry !OUTDIR! does not exist, attempting to create...
	MKDIR !OUTDIR!
	ECHO Output direcotry !OUTDIR! created.
)

ECHO Output direcotry: !OUTDIR!

ECHO Extracting metadata...
FOR %%f IN (!INDIR!\*.mp4) DO (
	SET INF=%%f
	SET METAF=!INDIR!\%%~nf_orig.txt
	%FFM% -y -i "!INF!" -f ffmetadata "!METAF!"
)
ECHO Done extracting metadata.

ECHO Calculating chapters...
%PYTHON% .\nminds-x4-submaker\src\submaker.py "!DATADIR!" "!TIMESF!" "!INDIR!" "!INDIR!"
ECHO Done calculating chapters.

FOR %%f IN (!INDIR!\*.mp4) DO (
	SET INF=%%f
	SET OUTF=!OUTDIR!\%%~nf_ch.mp4
	SET METAF=!INDIR!\%%~nf_chapters.txt
	ECHO Adding chapters to !INF!...
	%FFM% -y -i "!INF!" -i "!METAF!" -map_metadata 1 -codec copy "!OUTF!"
	COPY /Y !INDIR!\%%~nf.srt !OUTDIR!\%%~nf_ch.srt
	ECHO Done working on !INF!.
)

ECHO All done.