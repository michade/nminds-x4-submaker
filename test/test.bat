@echo off
set python=C:\Python27\python.exe
if exist .\out (
	rmdir .\out /S /Q
)
%python% ..\src\submaker.py .\res\data .\res\times.csv .\out .\res\metadata
echo "Checking for differences between expected and actual output:"
fc out\* expected\*
