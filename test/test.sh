#!/bin/bash
if [ -d ./out ]
then
    rm -rf ./out
fi
python ../src/submaker.py ./res/data ./res/times.csv ./out ./res/metadata
echo Checking for differences between expected and actual output:
diff out expected
