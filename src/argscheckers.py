"""
A library of validators to be used for argparse library.

Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

"""

import os
import stat
import argparse


class PathChecker(object):
    """
    Validates a file system object and its properties and access privilages.

    """

    def __init__(self, type_='df', mode='r'):
        """
        Initializes the object, which can then be passed to argparse's 'type' argument.

        :param type_: Type of the valid path target, a combination of 'd', 'f', and '+' in a single string.
                      The value 'd' indicates a directory, 'f' - regular file,
                      '+' - a possiblity of creating a new file or dir.
        :param mode: Access mode to target (ignored if target does not exist). Can be 'r' - read, 'w' - write,
                     or 'x' - execute.

        """
        self._file = 'f' in type_
        self._dir = 'd' in type_
        self._create = '+' in type_
        m = 0
        if 'r' in mode:
            m = m | os.R_OK
        if 'w' in mode:
            m = m | os.W_OK
        if 'x' in mode:
            m = m | os.X_OK
        self._mode = m

    def __call__(self, path):
        """
        Checks if path meets the criteria specified during object initialization.
        If not, raises argparse.ArgumentTypeError.

        :param path: The path to check.

        :returns: Unmodified path, if it meets the criteria.

        """
        if os.path.exists(path):
            st_mode = os.stat(path).st_mode
            if not((self._file and stat.S_ISREG(st_mode)) or (self._dir and stat.S_ISDIR(st_mode))):
                raise argparse.ArgumentTypeError('Invalid type of "{}".'.format(path))
            if not os.access(path, self._mode):
                raise argparse.ArgumentTypeError('Insufficient permissions for "{}".'.format(path))
        elif self._create:
            path_base = PathChecker._extract_existing(os.path.normpath(path))
            if not(os.path.isdir(path_base) and os.access(path_base, os.R_OK)):
                raise argparse.ArgumentTypeError('Insufficient permissions for "{}" (base path of "{}").'.format(path_base, path))

        else:
            raise argparse.ArgumentTypeError('Path invalid or malformed: "{}".'.format(path))
        return path

    @staticmethod
    def _extract_existing(path):
        """
        Get the part of "path" which exists.

        """
        s = (path,)
        while len(s[0]) > 0 and not os.path.exists(s[0]):
            s = os.path.split(s[0])
        if len(s[0]) == 0:
            return '.'
        else:
            return s[0]
