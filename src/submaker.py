"""
A program that parses interacting minds experiment data (nminds-x4)
and creates srt subtitles for the recordings, containing useful info.
Also creates chapters alinged with trials.

Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

submaker.py [-h] data_dir times_file output_dir [metadata_dir]

positional arguments:
  data_dir      Data files directory.
  times_file    A .csv file containing time offsets (recording vs program).
  output_dir    Output directory for the program results.
  metadata_dir  Directory with the pre-extracted metadata.

optional arguments:
  -h, --help  show this help message and exit

"""

import os
import csv
import re
import string
import argparse
from datetime import timedelta
from argscheckers import PathChecker


DATA_FILE_NAME_REGEX = re.compile('nminds\-exp\-\d{8}\-\d{4}\-(\d{3})\.txt')  # matches experiment data file name
VIDEO_FILE_NAME_REGEX = re.compile('(sesja(\d{3})\-.+)\.(mp4|avi|mkv|MTS)')  # matches experiment video recording file name
SUBTITLES_FILE_FMT = '{video_file_basename}.srt'
METADATA_ORIG_FILE_FMT = '{video_file_basename}_orig.txt'
METADATA_CHAPTERS_FILE_FMT = '{video_file_basename}_chapters.txt'

N_PRACTICE_TRIALS = 4  # number of trials in the practice block

MASTER_CLOCK_ERROR_CORRECTION = timedelta(seconds=-4)  # correct for the fact, that the master clock is synchronized with the *start* of the countdown, not with the *end*


def str_to_time(s):
    """
    Convert string representing number of seconds (as a floating-point number)
    to a datetime.timedelta object.

    :param s: The string to parse.

    :returns: A parsed timedelta object.

    """
    return timedelta(seconds=float(s))


class DataFileParser(object):
    """
    Extracts data (timing, decisions) from the experimental data file.

    """

    _DIALECT_NAME = 'nminds-x4-data'

    def __init__(self, file_):
        """
        Initializes the object instance and registers a dialect for the csv module.

        :param file_: An *open* data file.

        """
        self._file = file_
        if DataFileParser._DIALECT_NAME not in csv.list_dialects():
            csv.register_dialect(DataFileParser._DIALECT_NAME, delimiter='\t', quoting=csv.QUOTE_NONE)

    def __iter__(self):
        """
        Iterates over parsed csv rows, wrapped in Row to provide access to elements by column name.

        """
        try:
            reader = csv.reader(self._file, DataFileParser._DIALECT_NAME)
            cols = {h: i for i, h in enumerate(reader.next())}
        except:
            raise
        prev = None
        i = 0
        for r in reader:
            row = DataFileParser.Row(r, i, cols, prev)
            prev = row
            i = i + 1
            yield row

    class Row(object):
        """
        Used to wrap parsed csv rows to provide access to elements by column name.

        """

        def __init__(self, row, row_idx, cols, prev):
            """
            Initializes an object instance.

            :param row: The row data.
            :param row_idx: Index of the row in the file.
            :param cols: Column names as a dictionary { name: index }
            :param prev: Previuos row (a Row object).

            """
            self._row = row
            self._row_idx = row_idx
            self._cols = cols
            self._prev = prev

        def __getitem__(self, key):
            """
            Returns the data from a given column. If 'key' is a string, it is treated
            as a column name. If it is a tuple, the first element represents the base column name,
            the second - the participant number, to which the column pertains.

            :param key: the key for the column to retreive.

            :returns: The data value from the given column.

            """
            colname, ppant = key if isinstance(key, tuple) else (key, None)
            s = colname if ppant is None else 'p{}.{}'.format(ppant, colname)
            return self._row[self._cols[s]]

        @property
        def row(self):
            """
            The raw row data.

            """
            return self._row

        @property
        def prev(self):
            """
            The previous row, as a Row object.

            """
            return self._prev

        @property
        def row_idx(self):
            """
            Index of this row within a file.

            """
            return self._row_idx


class SubtitlesExtractor(object):
    """
    Formats subtitle texts and determines their timing.

    """

    SEP = ' ' * 5
    FMT_TRIAL = '{practice}{trial_idx:03d}{group}'
    FMT_DECS = FMT_TRIAL + SEP + 'A:{decs[0]} B:{decs[1]} C:{decs[2]} D:{decs[3]}'
    FMT_GROUP = FMT_DECS + SEP + 'group({group_dec_maker}):{group_dec}'

    def parse_row(self, row):
        """
        Extracts relevant data (timing, decisions) from the row source.
        Returns a list of tuples (start, end, text), that should be converted to subtitles:
        start - Subtitle start time (datetime.timedelta)
        end - Subtitle end time (datetime.timedelta)
        text - Subtitle text (string)

        :param row: The row (obtained from DataFileParser).

        :returns: A list of entries (start, end, text) - see above.

        """
        practice = row['practice'] == 'True'
        trial_idx = (row.row_idx if practice else row.row_idx - N_PRACTICE_TRIALS) + 1
        trial_start = str_to_time(row['trial_start']) + MASTER_CLOCK_ERROR_CORRECTION
        trial_finish = str_to_time(row['trial_finish']) + MASTER_CLOCK_ERROR_CORRECTION
        decs = [(
            i,  # ppant_no
            str_to_time(row['indiv_dec_time', i]) - str_to_time(row['trial_wait.offset', i]) + trial_start,  # correct for time difference on different machines
            int(row['indiv_dec', i]) + 1
        ) for i in range(4)]
        decs.sort(key=lambda x: x[1])  # sort by time, ascending
        group_dec_maker = row['group_dec_maker']
        group = group_dec_maker != 'NA'
        if group:
            i = int(group_dec_maker)
            group_dec_maker = string.ascii_uppercase[i]
            group_dec_start = str_to_time(row['group_dec_prompt.onset', i])
            group_dec_time = str_to_time(row['group_dec_time']) - str_to_time(row['trial_wait.offset', i]) + trial_start  # correct for time difference on different machines
        decs_str = ['_'] * 4
        subs = [
            self._format_text(SubtitlesExtractor.FMT_TRIAL, practice, trial_idx, group, decs_str),  # only trial no.
            self._format_text(SubtitlesExtractor.FMT_DECS, practice, trial_idx, group, decs_str)  # no indiv. decs made yet
        ]
        for i, _, dec in decs:
            decs_str[i] = dec
            subs.append(self._format_text(SubtitlesExtractor.FMT_DECS, practice, trial_idx, group, decs_str))  # indiv. decs show up
        if group:
            group_dec = int(row['group_dec']) + 1
            subs.append(self._format_text(SubtitlesExtractor.FMT_GROUP, practice, trial_idx, group, decs_str, group_dec_maker, '_'))  # group decision phase
            subs.append(self._format_text(SubtitlesExtractor.FMT_GROUP, practice, trial_idx, group, decs_str, group_dec_maker, group_dec))  # group decision made
        prev_finish = str_to_time(row.prev['trial_finish']) + MASTER_CLOCK_ERROR_CORRECTION if row.prev is not None else timedelta(seconds=1)
        times = [prev_finish, trial_start] + [t for _, t, _ in decs]
        if group:
            times.extend([group_dec_start, group_dec_time])
        times.append(trial_finish)
        data = zip(times[:-1], times[1:], subs)
        return data

    def _format_text(self, fmt, practice, trial_idx, group, decs, group_dec_maker='-', group_dec='-'):
        """
        Format a single subtitle text.

        :param fmt: The format string to use.
        :param practice: Was this trial a practice trial?
        :param trial_idx: Index of this trial.
        :param group: Was the group phase present in the trial?
        :param decs: The individual decisions of the participants.
        :param group_dec_maker: The letter code of the participant responsible for the group decision input.
        :param group_dec: The group decision.

        :returns: The formatted subtitle text.

        """
        s = fmt.format(
            practice='p' if practice else 'e',
            trial_idx=trial_idx,
            decs=decs,
            group_dec_maker=group_dec_maker,
            group_dec=group_dec,
            group='[G]' if group else '  '
        )
        return s


class SrtWriter(object):
    """
    Writes srt subitle files.

    """
    SUB_FMT = '{index}\n{start} --> {end}\n{text}\n\n'
    TIME_TUPLE_FMT = '{h:02d}:{m:02d}:{s:02d},{ms:03d}'

    def write(self, path, data, pad=1):
        """
        Writes a srt file with appropriately shifted timing.

        :param path: Path to output srt file.
        :param data: A collection of subtitle data items,
                     as obtained from DataFileParser (start_time, end_time, text).
                     Times are measured in seconds (float).
        :param pad: Time span in milliseconds (int) to keep between consecutive subtitles.

        """
        with open(path, 'w') as f:
            f.writelines([self._format_item(i, item, pad) for i, item in enumerate(data)])

    def _format_item(self, index, item, pad):
        """
        Format a single subtitle.

        :param index: Zero based index of the current item
        :param item: Subtitle data obtained from DataFileParser (start_time, end_time, text).
        :param pad: Time span in milliseconds (int) to keep between consecutive subtitles.

        """
        return SrtWriter.SUB_FMT.format(
            index=index,
            start=self._format_time(item[0]),  # start time
            end=self._format_time(item[1] - timedelta(milliseconds=1)),  # end time
            text=item[2])  # text

    def _format_time(self, td):
        """
        Format a time string.

        :param td: The datetime.timedelta object to format.

        :returns: The time as a string.

        """
        h = td.seconds // 3600
        m = (td.seconds // 60) % 60
        s = td.seconds % 60
        ms = td.microseconds // 1000
        return SrtWriter.TIME_TUPLE_FMT.format(h=h, m=m, s=s, ms=ms)  # end time


class ChaptersExtractor(object):
    """
    Adds chapters to a metadata file.

    """

    def __init__(self, pad_before=1.0, pad_after=0.0):
        """
        Initializes the object instace.

        :param pad_before: The subtitle to appear this musch seconds earlier (float).
        :param pad_after: Number of seconds (float) to extend the subtitles duration.

        """
        self._pad_before = timedelta(seconds=pad_before)
        self._pad_after = timedelta(seconds=pad_after)

    def parse_row(self, row):
        """
        Parse a single row of the data file.

        :param row: The DataFile.Row object, exposing the row to parse.

        :returns: The parsed data, as a tuple with following components:
                  1) The index of the trial.
                  2) Was this a practice trial?
                  3) The start time of the trial.
                  4) The end time of the trial.
                  5) Was a group decision phase present?
                  The tuple is wrapped in a list.

        """
        practice = row['practice'] == 'True'
        trial_idx = (row.row_idx if practice else row.row_idx - N_PRACTICE_TRIALS) + 1
        return [(
            trial_idx,
            practice,
            str_to_time(row['trial_start']) - self._pad_before,
            str_to_time(row['trial_finish']) + self._pad_after,
            row['group_dec_maker'] != 'NA'
        )]


class ChaptersWriter(object):
    """
    Adds chapters to a metadata file.

    """
    CHAPTER_FMT = '\n[CHAPTER]\nTIMEBASE=1/1000\nSTART={start}\nEND={end}\ntitle={practice}{trial_idx:03d}{group}\n'

    def write(self, orig_path, chapters_path, chapters):
        """
        Writes a srt file with appropriately shifted timing.

        :param orig_path: Path to the original metadata file.
        :param chapters_path: Path to the new metadata files, with chapters included.
        :param chapters: The chapter data to add; A list of tuples:
                         1) The index of the trial.
                         2) Was this a practice trial?
                         3) The start time of the trial.
                         4) The end time of the trial.
                         5) Was a group decision phase present?

        """
        header = []
        if os.path.exists(orig_path):
            with open(orig_path, 'r') as orig_f:
                for line in orig_f:
                    line = line.strip()
                    if len(line) == 0:
                        break
                    header.append(line)
        header = ['\n'.join(header) + '\n']
        with open(chapters_path, 'w') as chapters_f:
            chapters_f.writelines(header + [self._format_chapter(c) for c in chapters if self._is_valid(c)])

    def _format_chapter(self, chapter):
        """
        Format a metadata entry for a single chapter.

        :param chapter: The chapter to format (see: 'write' method).

        :returns: The formatted metadata (string).

        """
        trial_idx, practice, start, end, group = chapter
        return ChaptersWriter.CHAPTER_FMT.format(
            practice='p' if practice else 'e',
            trial_idx=trial_idx,
            start=ChaptersWriter._milliseconds(start),
            end=ChaptersWriter._milliseconds(end),
            group='[G]' if group else '')

    @staticmethod
    def _milliseconds(td):
        """
        Extract total number of milliseconds from a datetime.timedelta object.

        :param td: A datetime.timedelta object.

        :returns: The total number of milliseconds that, the object represents (integer).

        """
        return td.seconds * 1000 + td.microseconds // 1000

    def _is_valid(self, chapter):
        """
        Is the chapter data valid?

        :param chapter: The chapter data.

        :returns: Is the chapter data valid?

        """
        return chapter[2] >= timedelta(0, 0)  # start >= 0.0


def parse_time(s1):
    """
    Converts time from string (e.g. 11:04:3.123) to a datetime.timedelta object.

    :param s1: Time as string.

    :returns: Time as a datetime.timedelta.

    """
    spl = s1.split('.')
    s2, ms = spl if len(spl) == 2 else (s1, '0')
    ms = int(ms)
    spl = s2.split(':')
    h, m, s = (0, 0, 0)
    if len(spl) >= 3:
        h = int(spl[-3])
    if len(spl) >= 2:
        m = int(spl[-2])
    s = int(spl[-1])
    return timedelta(hours=h, minutes=m, seconds=s, milliseconds=ms)


def list_videos(times_file):
    """
    Reads video files and time offsets from a csv file.
    The file has two columns - file name and time offset (e.g. '3:07.490').

    :param times_file: Path to the times file.

    :returns: A list of tuples (video_file_name, session_id, time_offset).
              Video file name is without extension.
              Time is a datetime.timdedelta object.

    """
    with open(times_file, 'r') as f:
        reader = csv.reader(f)
        videos = []
        for r in reader:
            m = VIDEO_FILE_NAME_REGEX.match(r[0])
            if m is not None:
                videos.append((
                    m.group(1),  # file name without extension
                    m.group(2),  # session_id
                    parse_time(r[1])  # time offset
                ))
    return videos


class DataFile(object):
    """
    Represents an experimental data file.

    """

    def __init__(self, path, parser_class):
        """
        Initialize the object instance. Does not read the file data yet.

        :param path: The path to the file.
        :param parser_class: The class, whose instance will be used to parse the data file.

        """
        self._path = path
        self._is_extracted = False
        self._parser_class = parser_class

    @property
    def path(self):
        """
        The path to the file.

        """
        return self._path

    @property
    def is_extracted(self):
        """
        Has the file data already been extracted?

        """
        return self._is_extracted

    def extract_data(self, **on_next_row_callbacks):
        """
        Extract the file data. For each row, each of the callbacks passed will be called.
        A callback should return a list of results, that will be concatenated and stored,
        as the DataFile object attribute, with the same name as the respective callbacks'
        name in the keyword args of this method.

        :param on_next_row_callbacks: The callbacks (A callback must be a callable objects).

        """
        for key, _ in on_next_row_callbacks.items():
            setattr(self, key, [])
        with open(self._path, 'r') as f:
            parsed = self._parser_class(f)
            for row in parsed:
                for key, cb in on_next_row_callbacks.items():
                    getattr(self, key).extend(cb(row))


def find_data_files(data_dir):
    """
    Finds data files in the given directory, and extracts their session numbers.

    :param data_dir: Directory containing the data files.

    :returns: Dictionary { session_id : data_file_path }.

    """
    data_files = {}
    for filename in os.listdir(data_dir):
        m = DATA_FILE_NAME_REGEX.match(filename)
        path = os.path.join(data_dir, filename)
        if m is not None and os.path.isfile(path):
            data_files[m.group(1)] = DataFile(path, DataFileParser)
    return data_files


def main(data_dir, times_file, output_dir, metadata_dir):
    """
    Main routine of the program.

    :param data_dir: Data files directory.
    :param times_file: Time offsets file.
    :param output_dir: Output directory for the subtitles.
    :param metadata_dir: Directory with the pre-extracted metadata.

    """
    sub_extractor = SubtitlesExtractor()
    srt_writer = SrtWriter()
    do_chapters = metadata_dir is not None
    extractors = {
        'subtitles': sub_extractor.parse_row,
    }
    if do_chapters:
        chap_extractor = ChaptersExtractor()
        chap_writer = ChaptersWriter()
        extractors['chapters'] = chap_extractor.parse_row
    data_files = find_data_files(data_dir)
    videos = list_videos(times_file)
    for video_file, session_id, time_offset in videos:
        if session_id not in data_files:
            continue  # TODO: log
        df = data_files[session_id]
        if not df.is_extracted:
            df.extract_data(**extractors)
        video_file_basename = os.path.basename(video_file)
        subtitles_file = SUBTITLES_FILE_FMT.format(video_file_basename=video_file_basename)
        subs = [(timedelta(seconds=0), timedelta(seconds=1), 'START')] + df.subtitles
        subs = [(s + time_offset, e + time_offset, txt) for s, e, txt in subs]  # shift times by time_offset
        subs.insert(0, (timedelta(seconds=0), subs[0][0] - timedelta(milliseconds=1), session_id))
        srt_writer.write(os.path.join(output_dir, subtitles_file), subs)
        if do_chapters:
            metadata_orig_file = METADATA_ORIG_FILE_FMT.format(video_file_basename=video_file_basename)
            metadata_chapters_file = METADATA_CHAPTERS_FILE_FMT.format(video_file_basename=video_file_basename)
            chap_writer.write(
                os.path.join(metadata_dir, metadata_orig_file),
                os.path.join(output_dir, metadata_chapters_file),
                [(i, p, s + time_offset, e + time_offset, g) for i, p, s, e, g in df.chapters])  # shift times by time_offset


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='A program that parses interacting minds experiment data (nminds-x4)\nand creates srt subtitles for the recordings, containing useful info. Also creates chapters alinged with trials.')
    parser.add_argument('data_dir', help='Data files directory.', type=PathChecker('d'))
    parser.add_argument('times_file', help='A .csv file containing time offsets (recording vs program).', type=PathChecker('f'))
    parser.add_argument('output_dir', help='Output directory for the subtitles.', type=PathChecker('d+', 'w'))
    parser.add_argument('metadata_dir', nargs='?', help='Directory with the pre-extracted metadata.', type=PathChecker('d'))
    args = parser.parse_args()
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
    main(args.data_dir, args.times_file, args.output_dir, args.metadata_dir)
