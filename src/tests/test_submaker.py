"""

submaker.py - Unit tests

Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

"""

import pytest
import mock
from utils import MockFiles
from datetime import timedelta
from submaker import *


def patch_open(mock_obj=mock.mock_open(), module='submaker'):
    return mock.patch(module + '.open', mock_obj, create=True)


def assert_file_used(mo, path, mode):
    mo.assert_called_once_with(path, mode)
    assert mo.return_value.close.called or mo.return_value.__exit__.called


def get_written(mo):
    return ''.join([a[1] for a in mo.call_args_list()])


class Case1(object):

    @staticmethod
    def _make_ppant_col_names(s):
        return ['p{}.{}'.format(i, s) for i in range(4)]

    def __init__(self):
        self._data = [
            (
                True,
                100.0, 109.0,
                'NA',
                'NA',
                'NA', 'NA', 'NA', 'NA',
                0,
                101.1, 103.2, 102.3, 104.4,
                100.1, 100.2, 100.3, 100.4,
                0, 0, 0, 0
            ),
            (
                False,
                200.0, 209.0,
                3,
                206.0,
                -1.0, -1.0, -1.0, 207.0,
                1,
                201.1, 203.2, 202.3, 204.4,
                200.1, 200.2, 200.3, 200.4,
                1, 1, 1, 0
            )
        ]

        columns = [
            'practice',
            'trial_start',
            'trial_finish',
            'group_dec_maker',
            'group_dec_time',
            'group_dec'
        ]
        columns.extend(Case1._make_ppant_col_names('group_dec_promt.onset'))
        columns.extend(Case1._make_ppant_col_names('indiv_dec_time'))
        columns.extend(Case1._make_ppant_col_names('trial_wait.offset'))
        columns.extend(Case1._make_ppant_col_names('indiv_dec'))
        self._columns = columns

        def td(m, s, ms=0):
            return timedelta(minutes=m, seconds=s, milliseconds=ms)

        p1 = 'p001'
        p2 = 'e001[G]'
        sep = 5 * ' '
        self._subs_data = [
            (td(0, 1), td(1, 39, 999), p1),
            (td(1, 40), td(1, 40, 999), p1 + sep + 'A:_ B:_ C:_ D:_'),
            (td(1, 41), td(1, 41, 999), p1 + sep + 'A:1 B:_ C:_ D:_'),
            (td(1, 42), td(1, 42, 999), p1 + sep + 'A:1 B:_ C:1 D:_'),
            (td(1, 43), td(1, 43, 999), p1 + sep + 'A:1 B:1 C:1 D:_'),
            (td(1, 44), td(1, 44, 999), p1 + sep + 'A:1 B:1 C:1 D:1'),
            (td(1, 45), td(3, 19, 999), p2),
            (td(3, 20), td(3, 20, 999), p2 + sep + 'A:_ B:_ C:_ D:_'),
            (td(3, 21), td(3, 21, 999), p2 + sep + 'A:2 B:_ C:_ D:_'),
            (td(3, 22), td(3, 22, 999), p2 + sep + 'A:2 B:_ C:2 D:_'),
            (td(3, 23), td(3, 23, 999), p2 + sep + 'A:2 B:2 C:2 D:_'),
            (td(3, 24), td(3, 25, 999), p2 + sep + 'A:2 B:2 C:2 D:1'),
            (td(3, 26), td(3, 26, 999), p2 + sep + 'A:2 B:2 C:2 D:1' + sep + 'group(C):_'),
            (td(3, 27), td(3, 28, 999), p2 + sep + 'A:2 B:2 C:2 D:1' + sep + 'group(C):2')
        ]

        subs_srt = [
            '00:00:01,000 --> 00:01:39,999',
            '00:01:40,000 --> 00:01:40,999',
            '00:01:41,000 --> 00:01:42,999',
            '00:01:42,000 --> 00:01:43,999',
            '00:01:43,000 --> 00:01:44,999',
            '00:01:44,000 --> 00:01:45,999',
            '00:01:45,000 --> 00:03:19,999',
            '00:03:20,000 --> 00:03:20,999',
            '00:03:21,000 --> 00:03:21,999',
            '00:03:22,000 --> 00:03:22,999',
            '00:03:23,000 --> 00:03:23,999',
            '00:03:24,000 --> 00:03:25,999',
            '00:03:26,000 --> 00:03:26,999',
            '00:03:27,000 --> 00:03:28,999'
        ]
        self._subs_srt = [s + '\n' + self._subs_data[i][2] for i, s in enumerate(subs_srt)]

        str_rows = [[str(x) for x in dr] for dr in self._data]

        self._raw_rows = ['\t'.join(sr) for sr in str_rows]

        prev = None
        rows = []
        dict_cols = dict([(s, i) for i, s in enumerate(self._columns)])
        for i, dr in enumerate(str_rows):
            row = DataFileParser.Row(dr, i, dict_cols, prev)
            rows.append(row)
            prev = row
        self._rows = rows

        self._chapters_data = [
            (1, True, timedelta(minutes=1, seconds=40.0), timedelta(minutes=1, seconds=49.0), False),
            (2, False, timedelta(minutes=3, seconds=20.0), timedelta(minutes=3, seconds=29.0), True)
        ]

        self._chapters_txt = '\n'.join([
            '\n[CHAPTER]\nTIMEBASE=1/1000\nSTART=100000\nEND=109000\ntitle=p001\n',
            '\n[CHAPTER]\nTIMEBASE=1/1000\nSTART=200000\nEND=209000\ntitle=e002[G]\n'
        ])

        self._chapters_meta = 'test original\nmeta data'

    def mock_data_file(self):
        m = mock.mock_open(read_data='\n'.join(self.raw_rows))
        p = patch_open(m)
        return m, p

    @property
    def raw_rows(self):
        return self._raw_rows

    @property
    def rows(self):
        return self._rows

    @property
    def subs_data(self):
        return self._subs_data

    @property
    def subs_srt(self):
        return self._subs_srt

    @property
    def chapters_data(self):
        return self._chapters_data

    @property
    def chapters_txt(self):
        return self._chapters_txt

    @property
    def chapters_meta(self):
        return self._chapters_meta

    def finalize(self):
        pass


@pytest.fixture()
def case1(request):
    c = Case1()
    request.addfinalizer(c.finalize)
    return c


class Test_str_to_time(object):

    def test_str_to_time_positive(self):
        assert str_to_time('123.456') == timedelta(seconds=123, milliseconds=456)

    def test_str_to_time_negative(self):
        assert str_to_time('-123.456') == timedelta(seconds=-123, milliseconds=-456)

    def test_str_to_time_full_seconds(self):
        assert str_to_time('123.0') == timedelta(seconds=123)

    def test_str_to_time_only_milliseconds(self):
        assert str_to_time('0.456') == timedelta(milliseconds=456)


class Test_DataFileParser(object):

    _TEST_PATH = 'Test_DataFileParser'

    def test_init(self, case1):
        open_, _ = case1.mock_data_file()
        with open_(Test_DataFileParser._TEST_PATH, 'r') as f:
            DataFileParser(f)
        with open_(Test_DataFileParser._TEST_PATH, 'r') as f:
            DataFileParser(f)
        assert True

    def test_iter(self, case1):
        open_, _ = case1.mock_data_file()
        with open_(Test_DataFileParser._TEST_PATH, 'r') as f:
            dfp = DataFileParser(f)
            rows = list(dfp)
        assert rows == case1.rows

    def test_empty_file(self):
        open_ = mock_open()
        with open_:
            dfp = DataFileParser(f)
            rows = list(dfp)
        assert rows == []


class Test_DataFileParser_Row(object):

    @pytest.mark.parametrize('index,column,value', [
        (0, 'group_dec_maker', 'NA'),
        (1, 'group_dec_maker', '3'),
        (0, 'practice', 'True'),
        (1, 'practice', 'False'),
        (0, 'trial_start', '100.0'),
        (1, 'trial_start', '200.0')
    ])
    def getitem(self, case1, index, column, value):
        assert case1.rows[index][column] == value

    @pytest.mark.parametrize('index,column,ppant,value', [
        (0, 'indiv_dec', 2, '0'),
        (0, 'indiv_dec', 3, '0'),
        (1, 'indiv_dec', 2, '0'),
        (1, 'indiv_dec', 3, '1'),
        (0, 'trial_wait.offset', 1, '100.1'),
        (0, 'trial_wait.offset', 2, '100.2'),
        (1, 'trial_wait.offset', 1, '200.1'),
        (1, 'trial_wait.offset', 2, '200.2'),
    ])
    def getitem_complex(self, case1, index, column, ppant, value):
        assert case1.rows[index][column, ppant] == value

    def row(self, case1):
        assert case1.rows[0].row == case1.raw_rows[0].split('\t')
        assert case1.rows[1].row == case1.raw_rows[1].split('\t')

    def row_idx(self, case1):
        assert case1.rows[0].row_idx == 0
        assert case1.rows[1].row_idx == 1

    def test_prev_row_first(self, case1):
        assert case1.rows[0].prev is None

    def test_prev_row_not_first(self, case1):
        assert case1.rows[1].prev is case1.rows[0]


class Test_SubtitlesExtractor(object):

    def test_parse_row_first(self, case1):
        sx = SubtitlesExtractor()
        parsed = sx.parse_row(case1.rows[0])
        assert parsed == case1.subs_data[0]

    def test_parse_row_not_first(self, case1):
        sx = SubtitlesExtractor()
        parsed = sx.parse_row(case1.rows[1])
        assert parsed == case1.subs_data[1]


class Test_SrtWriter(object):

    @pytest.fixture
    def mock_output(self):
        mo = mock.mock_open()
        p = patch_open(mo)
        return mo, p

    def test_write(self, case1, mock_output):
        w = SrtWriter()
        path = 'Test_SrtWriter'
        open_, open_patch = mock_output
        with open_patch:
            w.write(path, case1.subs_data)
        assert_file_used(mo, path, 'w')
        assert get_written(mo) == case1.subs_srt

    def test_write_empty(self, mock_output):
        w = SrtWriter()
        path = 'Test_SrtWriter'
        open_, open_patch = mock_output
        with open_patch:
            w.write(path, [])
        assert_file_used(mo, path, 'w', written='')
        assert get_written(mo) == ''


class Test_ChaptersExtractor(object):

    def test_parse_row_first(self, case1):
        cx = ChaptersExtractor()
        parsed = cx.parse_row(case1.rows[0])
        assert parsed == case1.chapters_data[0]

    def test_parse_row_not_first(self, case1):
        cx = ChaptersExtractor()
        parsed = cx.parse_row(case1.rows[1])
        assert parsed == case1.chapters_data[1]


class Test_ChaptersWriter(object):

    @pytest.fixture(params=[None, 'meta'])
    def files_setup(self, request, case1):
        path = 'Test_ChaptersWriter'
        files = []
        if request.param:
            files = [(request.param, case1.chapters_meta)]
        else:
            files = []
        return MockFiles(root=path, files=files), os.path.join(path, request.param)

    def test_write(self, case1, files_setup):
        mf, path_meta = files_setup
        file_out = 'out'
        path_out = os.path.join(mf.root, file_out)
        w = ChaptersWriter()
        with mf:
            w.write(path_meta, path_out, case1.chapters_data)
        assert_file_used(mf, path_out, 'w')
        if path_meta:
            assert_file_used(mf, path_meta, 'r')
            assert get_written(mf.files[file_out].mock) == case1.chapters_meta + '\n' + case1.chapters_text
        else:
            assert get_written(mf.files[file_out].mock) == case1.chapters_text

    def test_write_empty(self, case1, files_setup):
        mf, path_meta = files_setup
        file_out = 'out'
        path_out = os.path.join(mf.root, file_out)
        w = ChaptersWriter()
        with mf:
            w.write(path_meta, path_out, [])
        assert_file_used(mf, path_out, 'w')
        if path_meta:
            assert_file_used(mf, path_meta, 'r')
            assert get_written(mf.files[file_out].mock) == case1.chapters_meta
        else:
            assert get_written(mf.files[file_out].mock) == ''


class Test_parse_time(object):

    def test_up_to_seconds(self):
        assert parse_time('45.123') == timedelta(seconds=45, milliseconds=123)

    def test_up_to_minutes(self):
        assert parse_time('17:45.123') == timedelta(minutes=17, seconds=45, milliseconds=123)

    def test_up_to_hours(self):
        assert parse_time('9:17:45.123') == timedelta(hours=9, minutes=17, seconds=45, milliseconds=123)

    def test_up_to_milliseconds_trailing(self):
        assert parse_time('00:00:00.123') == timedelta(milliseconds=123)

    def test_up_to_seconds_trailing(self):
        assert parse_time('00:00:45.123') == timedelta(seconds=45, milliseconds=123)

    def test_up_to_minutes_trailing(self):
        assert parse_time('00:17:45.123') == timedelta(minutes=17, seconds=45, milliseconds=123)

    def test_up_to_hours_trailing(self):
        assert parse_time('09:17:45.123') == timedelta(hours=9, minutes=17, seconds=45, milliseconds=123)

    def test_many_hours(self):
        assert parse_time('54321:17:45.123') == timedelta(hours=54321, minutes=17, seconds=45, milliseconds=123)

    def test_all_negative(self):
        assert parse_time('-9:-17:-45.-123') == timedelta(hours=-9, minutes=-17, seconds=-45, milliseconds=-123)

    def test_some_negative(self):
        assert parse_time('00:10:-15.000') == timedelta(minutes=10, seconds=-15)

    def test_no_milliseconds(self):
        assert parse_time('09:17:45') == timedelta(hours=9, minutes=17, seconds=45)

    def test_zero(self):
        assert parse_time('00:00:00.000') == timedelta()


class Test_list_videos(object):

    _TEST_TIMES_FILE_PATH = 'Test_list_videos.times_file'

    @pytest.mark.xfail(reason="cannot mock input file for csv.reader")
    def test_not_empty_file(self):
        csv_data = 'sesja099-abc.mp4,23.456\nsesja123-asdabc.MTS,3:-21:45.678\nsesja099-def.avi,43:12.987'
        expected = [
            ('sesja099-abc', '099', timedelta(seconds=23, milliseconds=456)),
            ('sesja123-asdabc', '123', timedelta(minutes=3, seconds=-21, milliseconds=678)),
            ('sesja099-def', '099', timedelta(minutes=43, seconds=12, milliseconds=987))
        ]
        path = Test_list_videos._TEST_TIMES_FILE_PATH
        mo = mock.mock_open(read_data=csv_data)
        with patch_open(mo):
            vl = list_videos(path)
        assert_file_used(mo, path, 'r')
        assert vl == expected

    @pytest.mark.xfail(reason="cannot mock input file for csv.reader")
    def test_empty_file(self):
        path = Test_list_videos._TEST_TIMES_FILE_PATH
        mo = mock.mock_open()
        with patch_open(mo):
            vl = list_videos(path)
        assert_file_used(mo, path, 'r')
        assert vl == []


class Test_DataFile(object):

    _TEST_PATH = 'Test_DataFile'

    @pytest.fixture
    def data_file(self):
        mock_parser = mock.MagicMock()
        mock_parser_class = mock.create_autospec(DataFileParser, return_value=mock_parser)
        path = Test_DataFile._TEST_PATH
        df = DataFile(path, mock_parser_class)
        open_ = mock_open()
        open_patch = patch_open(open_)
        return df, mock_parser_class, mock_parser, open_patch, open_

    def test_init(self):
        DataFile(Test_DataFile._TEST_PATH, DataFileParser)
        assert True

    def test_path(self):
        path = Test_DataFile._TEST_PATH
        df = DataFile(path, DataFileParser)
        assert df.path == path

    def test_is_extracted(self, data_file):
        df, _, _, open_patch, open_ = data_file
        with open_patch:
            df.extract_data()
        assert df.is_extracted

    def test_parser_created(self, data_file):
        df, mpcls, mp, open_patch, open_ = data_file
        with open_patch:
            df.extract_data()
        assert 0

    def test_file_opened(self, data_file):
        df, mpcls, mp, open_patch, open_ = data_file
        with open_patch:
            df.extract_data()
        assert 0

    def test_parser_works(self, data_file):
        df, mpcls, mp, open_patch, open_ = data_file
        itermock = mock.Mock(iter([]), wraps=range(3))
        mp.__iter__.return_value = itermock
        with open_patch:
            df.extract_data()
        assert itemock.call_count == 3 + 1

    def test_attributes_after_extract(self, data_file):
        df, mpcls, mp, open_patch, open_ = data_file
        old_dir = dir(df)
        with open_patch:
            df.extract_data(e1=lambda s: [1, 2, 3], e2=lambda s: [4, 5, 6])
        old_dir.append('e1')
        old_dir.append('e2')
        assert sorted(dir(df)) == sorted(old_dir)

    def test_rows_passed_to_persers(self, data_file):
        df, mpcls, mp, open_patch, open_ = data_file
        mp.__iter__.return_value = iter([1, 2, 3])
        e1 = mock.Mock(return_value=[])
        e2 = mock.Mock(return_value=[])
        with open_patch:
            df.extract_data(e1=e1, e2=e2)
        e1.assert_called(1)
        e1.assert_called(2)
        e1.assert_called(3)
        e2.assert_called(1)
        e2.assert_called(2)
        e2.assert_called(3)

    def test_extract_results_single(self, data_file):
        df, mpcls, mp, open_patch, open_ = data_file
        mp.__iter__.return_value = iter([1, 2, 3])
        e1 = mock.Mock(side_effect=[[4], [5], [6]])
        e2 = mock.Mock(side_effect=[[7], [8], [9]])
        with open_patch:
            df.extract_data(e1=e1, e2=e2)
        assert df.e1 == [4, 5, 6]
        assert df.e2 == [7, 8, 9]

    def test_extract_results_multiple(self, data_file):
        df, mpcls, mp, open_patch, open_ = data_file
        mp.__iter__.return_value = iter([1, 2, 3])
        e1 = mock.Mock(side_effect=[[4], [5, 6], [7]])
        e2 = mock.Mock(side_effect=[[7, 8], [9], [10, 11, 12]])
        with open_patch:
            df.extract_data(e1=e1, e2=e2)
        assert df.e1 == [4, 5, 6, 7]
        assert df.e2 == [7, 8, 9, 10, 11, 12]


@pytest.fixture
def sample_setup():
    data_files = [
        'nminds-exp-12345678-1234-099.txt',
        'nminds-exp-12345678-1234-098.txt',
        'nminds-exp-12345678-1234-097.txt'
    ]
    mf = MockFiles(
        root='foo',
        files=[
            data_files[0],
            data_files[1],
            'afile',
            'xnminds-exp-12345678-1234-099.txt',
            data_files[2]
        ],
        dirs=[
            'adir',
            'nminds-exp-12345678-1234-097.txt'
        ]
    )
    return mf, data_files


class Test_find_data_files(object):

    def test_find_data_files(self, sample_setup):
        mf, exp_files = sample_setup
        with mf:
            data_files = find_data_files(mf.root)
        expected = {df[-7:-4]: os.path.join(mf.root, df) for df in exp_files}
        res = {k: v.path for k, v in data_files.items()}
        assert res == expected
