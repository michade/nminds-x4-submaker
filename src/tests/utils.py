"""

Helper classes for mocking a directory contents.

Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

"""

import sys
import mock
import os.path


class MockFile(object):

    def __init__(self, name, contents=''):
        self.name = name
        self.contents = contents
        self.mock = mock.mock_open(read_data=self.contents)
        self.mock.return_value.__iter__ = lambda self: self
        self.mock.return_value.__next__ = lambda self: self.readline()


class MockFiles(object):

    def __init__(self, root='.', files=None, dirs=None):
        self._root = os.path.normpath(root)
        self._files = {}
        for f in files:
            if isinstance(f, basestring):
                self._files[f] = MockFile(f)
            else:
                self._files[f[0]] = MockFile(*f)
        self._dirs = set(dirs) if dirs else set()
        self._patches = []
        self._make_mock('os.listdir', self._os_listdir)
        self._make_mock('os.path.exists', self._op_exists)
        self._make_mock('os.path.isfile', self._op_isfile)
        self._make_mock('os.path.isdir', self._op_isdir)
        self._make_mock('open', self._open)

    def __enter__(self):
        for p in self._patches:
            p.__enter__()

    def __exit__(self, exc_type, exc_val, exc_tb):
        for p in self._patches:
            p.__exit__(exc_type, exc_val, exc_tb)

    def _make_mock(self, target, mock_fun):
        tmp = target.rsplit('.', 1)
        if len(tmp) == 1:
            fun = getattr(sys.modules['__builtin__'], tmp[0])
            target = '__main__.' + target
        else:
            fun = getattr(sys.modules[tmp[0]], tmp[1])

        def _wrapper(path):
            path = os.path.normpath(path)
            if os.path.commonprefix([path, self._root]) == self._root:
                return mock_fun(os.path.basename(path))  # will only work for root directory
            else:
                return fun(path)

        mck = mock.Mock(side_effect=_wrapper)
        ptch = mock.patch(target, mck, create=len(tmp) == 1)
        name = target.replace('.', '_')
        setattr(self, 'mock_' + name, mck)
        self._patches.append(ptch)

    def _os_listdir(self, path):
        return self._files.keys() + list(self._dirs)

    def _op_exists(self, path):
        return path in self._files or path in self._dirs

    def _op_isfile(self, path):
        return path in self._files

    def _op_isdir(self, path):
        return path in self._dirs

    def _open(self, path, *args):
        return self._files[path].omock(path, mode, *args)

    @property
    def root(self):
        return self._root

    @property
    def files(self):
        return self._files

    @property
    def dirs(self):
        return self._dirs
