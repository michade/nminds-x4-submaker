Readme
======

Program
-------

- Name: nminds-x4-submaker
- Version: 1.0
- Code : src/

Authors
-------

Michal Denkiewicz (michal.denkiewicz@gmail.com)

License
-------

Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
All rights reserved.

Full license text (in Polish) can be found in LICENSE.polish and in the
user documentation accompanying the program.

Description
-----------

The program nminds-x4-submaker is a supplementray tool to the 'nminds-x4' program, used to conduct psychological experiments pertaining to decision making in four-person groups (for more details see the 'nminds-x4' documentation).

The program extracts data from the experiment data files, and creates subtitles and chapters metadata for the video recordings. These subtitles and chapters facilitate the reviewing of the video material.

Requirements
------------

- Python 2.7.6 (32bit)

Usage
-----
::
   submaker.py [-h] data_dir times_file output_dir [metadata_dir]

   positional arguments:
      data_dir      Data files directory.
      times_file    A .csv file containing time offsets (recording vs program).
      output_dir    Output directory for the program results.
      metadata_dir  Directory with the pre-extracted metadata. Skipping this parameter will prevent the program from creating chapters metadata.

   optional arguments:
      -h, --help  show this help message and exit

For an example of usage, see the test/ directory.

Documentation
-------------

Installation instructions and user documentation (in Polish) are in the file manual/build/instrukcja.pdf (or can be built from LaTeX sources using "make manual").

Programmer's documentation (must be built using the Sphinx tool):

- doc/build/html/index.html - html format
- doc/build/latex/nminds-x4-submaker.pdf - pdf format

Programmer's documentation sources are located in doc/source directory.

Tests
-----

A simple test fixture is provided in the test" directory, along with some mock data. The test scripts will generate output data in the "out" directory, and compare it with the expected output in the "expected" directory using diff (UNIX) or fc (Windows).

Unit tests, using py.test, are located in the src/test directory, and can be invoked by: "make test".

Miscallanea
-----------

The misc directory contains the following items:

- Windows utility scripts used (in conjunction with ffmpeg, https://www.ffmpeg.org/) to convert the experiment recordings. The scripts need to be modified by inserting appropriate paths in order to be used.
- The nminds-x4-submaker program output for the recordings from the original experiment.
- The times.csv file containing the time offsets between nminds-x4 program clock and the video recording time (from the original experiment).
